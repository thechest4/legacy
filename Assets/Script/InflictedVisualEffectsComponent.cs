﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InflictedVisualEffectsComponent : Photon.PunBehaviour
{
	[SerializeField]
	private StateContainerBase stateContainer;

	[SerializeField]
	private GameObject modelParent;

	[SerializeField]
	private Animator animator;

	private List<Texture> cachedTextures;

	[SerializeField]
	private bool canFlinch = false;

	// Use this for initialization
	void Start ()
	{
		stateContainer.OnDamageTaken 	+= HandleOnDamageTaken;
		stateContainer.OnDeath			+= HandleOnDeath;

		cachedTextures = new List<Texture> ();

		CacheRendererTextures ();
	}

	private void HandleOnDamageTaken ()
	{
		StopAllCoroutines ();
		ResetAllEffects ();

		if (canFlinch && photonView.isMine)
		{
			StartCoroutine (FlinchCoroutine ());
		}
		else
		{
			StartCoroutine (DamageFlashCoroutine ());
		}
	}

	private void HandleOnDeath ()
	{
		StopAllCoroutines ();
		ResetAllEffects ();

		StartCoroutine (DamageFlashCoroutine ());

		if (photonView.isMine)
		{
			animator.SetTrigger (AnimationKeys.DIE_TRIGGER);
			stateContainer.currentActionState = ActionState.Busy;
		}
	}

	private IEnumerator FlinchCoroutine ()
	{
		animator.SetTrigger (AnimationKeys.FLINCH_TRIGGER);

		stateContainer.currentActionState = ActionState.Busy;

		yield return new WaitForSeconds (0.1f);

		StartCoroutine (DamageFlashCoroutine ());

		//THIS WILL NOT GUARANTEE THE CORRECT DURATION!!!!
		yield return new WaitForSeconds (animator.GetCurrentAnimatorClipInfo (0)[0].clip.length - 0.2f);

		stateContainer.currentActionState = ActionState.Free;
	}

	private IEnumerator DamageFlashCoroutine ()
	{
		Renderer[] modelRenderers = modelParent.GetComponentsInChildren<Renderer> ();

		for (int i = 0; i < modelRenderers.Length; i++)
		{
			for (int j = 0; j < modelRenderers[i].materials.Length; j++)
			{
				modelRenderers[i].materials[j].mainTexture = null;
			}
		}

		yield return new WaitForSeconds (0.1f);

		for (int i = 0; i < modelRenderers.Length; i++)
		{
			for (int j = 0; j < modelRenderers[i].materials.Length; j++)
			{
				modelRenderers[i].materials[j].mainTexture = cachedTextures[i];
			}
		}
	}

	private void ResetAllEffects ()
	{
		Renderer[] modelRenderers = modelParent.GetComponentsInChildren<Renderer> ();

		for (int i = 0; i < modelRenderers.Length; i++)
		{
			for (int j = 0; j < modelRenderers[i].materials.Length; j++)
			{
				modelRenderers[i].materials[j].mainTexture = cachedTextures[i];
			}
		}
	}

	void OnDestroy ()
	{
		stateContainer.OnDamageTaken 	-= HandleOnDamageTaken;
		stateContainer.OnDeath			-= HandleOnDeath;
	}

	private void CacheRendererTextures ()
	{
		Renderer[] modelRenderers = modelParent.GetComponentsInChildren<Renderer> ();

		for (int i = 0; i < modelRenderers.Length; i++)
		{
			//Currently this only supports a single texture per material, will probably want to support this down the line
			for (int j = 0; j < modelRenderers[i].materials.Length; j++)
			{
				cachedTextures.Add (modelRenderers[i].materials[j].mainTexture);
			}
		}
	}
}

