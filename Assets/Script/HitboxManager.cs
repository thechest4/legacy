﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Each attack or ability with hitboxes will have one hitbox manager component who's methods are called via the HitboxManagerParser
/// </summary>
public class HitboxManager : MonoBehaviour
{
	[SerializeField]
	private List<Hitbox> hitboxes;

	public bool isActivated { get; set; }

	void Start ()
	{
		isActivated = false;
	}

	public void ActivateHitbox (int index)
	{
		for (int i = 0; i < hitboxes.Count; i++)
		{
			if (i == index)
			{
				hitboxes[index].gameObject.SetActive (true);
			}
			else
			{
				hitboxes[i].gameObject.SetActive (false);
			}
		}
	}

	public void DeactivateHitboxes ()
	{
		for (int i = 0; i < hitboxes.Count; i++)
		{
			hitboxes[i].gameObject.SetActive (false);
		}
	}

	public List<Hitbox> GetHitboxes ()
	{
		return hitboxes;
	}
}

