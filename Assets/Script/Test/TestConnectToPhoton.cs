﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestConnectToPhoton : Photon.PunBehaviour
{
	[SerializeField]
	private List<Transform> playerSpawnPoints;

	[SerializeField]
	private Transform enemySpawnPoint;

	bool joinedLobbyFlag = false;

	// Use this for initialization
	void Start ()
	{
		PhotonNetwork.ConnectUsingSettings ("");
	}

	void Update ()
	{
		if (joinedLobbyFlag && PhotonNetwork.connectedAndReady && PhotonNetwork.room == null)
		{
			PhotonNetwork.JoinOrCreateRoom ("Test Room", new RoomOptions (), new TypedLobby ());
		}
	}

	public override void OnConnectedToPhoton ()
	{
		Debug.LogError ("OnConnectedToPhoton");
//		PhotonNetwork.JoinLobby ();
	}

	public override void OnJoinedLobby ()
	{
		Debug.LogError ("OnJoinedLobby");
		joinedLobbyFlag = true;
//		PhotonNetwork.JoinOrCreateRoom ("Test Room", new RoomOptions (), new TypedLobby ());
	}

	public override void OnJoinedRoom ()
	{
		Debug.LogError ("OnJoinedRoom " + PhotonNetwork.room.name);

		if (PhotonNetwork.isMasterClient)
		{
//			PhotonNetwork.Instantiate ("Skeleton", enemySpawnPoint.position, Quaternion.identity, 0);
		}

		PhotonNetwork.Instantiate ("Necromancer", playerSpawnPoints[PhotonNetwork.player.ID - 1].position, Quaternion.identity, 0);
	}
}

