﻿using UnityEngine;
using System.Collections;

public interface IActionManager 
{
	void FinishCurrentAction ();

	void CurrentActionCreateProjectile ();
}

[System.Serializable]
public class ActionManagerContainer : IUnifiedContainer<IActionManager> { }