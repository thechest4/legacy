﻿using UnityEngine;
using System.Collections;

public class BasicCameraScript : MonoBehaviour
{
	[SerializeField]
	private Vector3 cameraOffset;

	private GameObject target;
	
	// Update is called once per frame
	void Update ()
	{
		if (target == null)
		{
			if (PlayerReferenceHolder.PlayerReferences.Count > 0 && PlayerReferenceHolder.PlayerReferences[0] != null)
			{
				target = PlayerReferenceHolder.PlayerReferences[0];
			}
		}
	}

	void LateUpdate ()
	{
		if (target != null)
		{
			transform.position = target.transform.position + cameraOffset;
		}
	}
}

