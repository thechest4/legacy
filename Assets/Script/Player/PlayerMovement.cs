﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Player basic movement such as walking and turning are handled here.
/// </summary>
public class PlayerMovement : Photon.PunBehaviour
{
	private const float baseSpeed = 1.0f;
	private float currentSpeed = 0.0f;

	[SerializeField]
	private PlayerStateContainer playerStateContainer;

	[SerializeField]
	private Rigidbody body;

	[SerializeField]
	private Animator animator;

	private const float rotationSpeed = 10.0f;

	// Use this for initialization
	void Start ()
	{
		if (!photonView.isMine)
		{
			this.enabled = false;
		}

		currentSpeed = baseSpeed;
	}

	//We use FixedUpdate specifically for movement because the movement feels bad if the framerate fluctuates too much, due to the player expecting constant velocity
	void FixedUpdate ()
	{
		if (playerStateContainer.currentActionState == ActionState.Free)
		{
			PlayerInputState currentInputState = playerStateContainer.currentInputState;

			Vector3 horzTranslation = Vector3.right;
			horzTranslation.y = 0.0f;
			horzTranslation *= currentInputState.horizontalAxis;

			Vector3 vertTranslation = Vector3.forward;
			vertTranslation.y = 0.0f;
			vertTranslation *= currentInputState.verticalAxis;

			Vector3 translation = horzTranslation + vertTranslation;
			translation = translation.normalized;
			translation *= currentSpeed * Time.fixedDeltaTime;

			body.MovePosition (transform.position + translation);

			if (translation.normalized.magnitude != 0.0f)
			{				
			    Quaternion lookRotation = Quaternion.LookRotation(translation.normalized);
			 
				transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, rotationSpeed * Time.fixedDeltaTime);
			}

			if (Mathf.Abs (currentInputState.horizontalAxis) == 0.0f && Mathf.Abs (currentInputState.verticalAxis) == 0.0f)
			{
				animator.SetBool (AnimationKeys.MOVEMENTAXIS_BOOL, false);
			}
			else
			{
				animator.SetBool (AnimationKeys.MOVEMENTAXIS_BOOL, true);
			}
		}
		else
		{
			animator.SetBool (AnimationKeys.MOVEMENTAXIS_BOOL, false);
		}
	}
}

