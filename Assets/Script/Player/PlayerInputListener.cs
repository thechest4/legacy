﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class listens to input and is responsible for updating the input state in the PlayerStateContainer.
/// This will be modified/replaced if we decide to use a plugin such as InControl to manage inputs, but until then it will be handled here.
/// </summary>
public class PlayerInputListener : Photon.PunBehaviour 
{
	[SerializeField]
	private PlayerStateContainer playerStateContainer;
	
	// Update is called once per frame
	void Update () 
	{
		if (photonView.isMine)
		{
			float horzInput = Input.GetAxisRaw ("Horizontal");
			float vertInput = Input.GetAxisRaw ("Vertical");
			bool leftClick 	= Input.GetMouseButtonDown(0);

			PlayerInputState newInputState = new PlayerInputState (horzInput, vertInput, leftClick);
			playerStateContainer.currentInputState = newInputState;
		}
	}
}

public struct PlayerInputState
{
	public float horizontalAxis;
	public float verticalAxis;
	public bool attackWasPressed;

	public PlayerInputState (float argHorizontalAxis, float argVerticalAxis, bool argAttackWasPressed)
	{
		horizontalAxis 		= argHorizontalAxis;
		verticalAxis 		= argVerticalAxis;
		attackWasPressed 	= argAttackWasPressed;
	}
}