﻿using UnityEngine;
using System.Collections;

/// <summary>
/// An interface that contains common functionality that any player action needs, such as the ability to be interrupted.
/// </summary>
public interface IPlayerControlledAction
{
	void Initialize (PlayerStateContainer argPlayerStateContainer, Animator argAnimator);

	void InterruptAction ();

	void PerformAction ();

	void FinishAction ();

	bool HasInputStateBeenMet ();
}

[System.Serializable]
public class PlayerActionContainer : IUnifiedContainer<IPlayerControlledAction> { }