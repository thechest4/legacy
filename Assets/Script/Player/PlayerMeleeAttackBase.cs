﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This is the base class for player attacks.  All player attacks should inherit this class
/// </summary>
public class PlayerMeleeAttackBase : MonoBehaviour, IPlayerControlledAction
{
	[SerializeField]
	protected HitboxManager hitboxManager;

	protected List<Hurtbox> hitHurtboxes;

	protected PlayerStateContainer playerStateContainer;
	protected Animator animator;

	public void Initialize (PlayerStateContainer argPlayerStateContainer, Animator argAnimator)
	{
		playerStateContainer 	= argPlayerStateContainer;
		animator				= argAnimator;

		hitHurtboxes = new List<Hurtbox> ();

		List<Hitbox> hitboxes = hitboxManager.GetHitboxes ();

		for (int i = 0; i < hitboxes.Count; i++)
		{
			hitboxes[i].Initialize (HitboxOwner.Player);
			hitboxes[i].OnHurtboxHit += HandleOnHurtboxHit;
		}
	}

	public void PerformAction ()
	{
		playerStateContainer.currentActionState = ActionState.Busy;
		animator.SetTrigger (AnimationKeys.MELEEATTACK_TRIGGER);
		hitboxManager.isActivated = true;
		hitHurtboxes.Clear ();
	}

	public void FinishAction ()
	{
		playerStateContainer.currentActionState = ActionState.Free;
		hitboxManager.isActivated = false;
		hitboxManager.DeactivateHitboxes ();
	}

	public virtual void InterruptAction ()
	{
		FinishAction ();
	}

	protected virtual void HandleOnHurtboxHit (Hurtbox hitHurtbox) 
	{
		if (!hitHurtboxes.Contains (hitHurtbox))
		{
			//Can probably even move this out to the base class once damage can be calculated by stats
			hitHurtbox.ApplyDamageToStateContainer (2);
			hitHurtboxes.Add (hitHurtbox);
		}
	}

	protected virtual void Shutdown () 
	{
		List<Hitbox> hitboxes = hitboxManager.GetHitboxes ();

		for (int i = 0; i < hitboxes.Count; i++)
		{
			hitboxes[i].OnHurtboxHit -= HandleOnHurtboxHit;
		} 
	}

	void OnDestroy ()
	{
		Shutdown ();
	}

	public bool HasInputStateBeenMet ()
	{
		return true;
	}
}

