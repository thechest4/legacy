﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerProjectileAttackBase : MonoBehaviour, IPlayerControlledAction, IProjectileAction
{
	[SerializeField]
	protected Transform projectileSpawnPoint;

	[SerializeField]
	protected string projectileResourceName;

	protected PlayerStateContainer playerStateContainer;
	protected Animator animator;

	public void Initialize (PlayerStateContainer argPlayerStateContainer, Animator argAnimator)
	{
		playerStateContainer 	= argPlayerStateContainer;
		animator				= argAnimator;
	}

	public void PerformAction ()
	{
		playerStateContainer.currentActionState = ActionState.Busy;
		animator.SetTrigger (AnimationKeys.PROJATTACK_TRIGGER);
	}

	public void FinishAction ()
	{
		playerStateContainer.currentActionState = ActionState.Free;
	}

	public virtual void InterruptAction ()
	{
		FinishAction ();
	}

	protected virtual void HandleOnHurtboxHit (Hurtbox hitHurtbox) 
	{
		hitHurtbox.ApplyDamageToStateContainer (2);
	}

	protected virtual void Shutdown () { }

	void OnDestroy ()
	{
		Shutdown ();
	}

	public bool HasInputStateBeenMet ()
	{
		return true;
	}

	public virtual void CreateProjectile ()
	{
		ProjectileBase spawnedProjectile = PhotonNetwork.Instantiate (projectileResourceName, projectileSpawnPoint.position, Quaternion.identity, 0).GetComponent<ProjectileBase> ();
		spawnedProjectile.Initialize (transform.forward);
		spawnedProjectile.OnProjectileHitHurtbox += HandleOnHurtboxHit;
	}
}

