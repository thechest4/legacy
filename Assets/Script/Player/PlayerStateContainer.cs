﻿using UnityEngine;
using System.Collections;

/// <summary>
/// All player state information is stored here, and all player components check this component for state information.
/// </summary>
public class PlayerStateContainer : StateContainerBase 
{
	public PlayerInputState currentInputState { get; set; }

	protected override void Die ()
	{
		//Die!

//		GameObject.Destroy (gameObject);
		PhotonNetwork.Destroy (gameObject);
	}

	protected override void SetHurtboxOwnerType ()
	{
		hurtboxOwnerType = HurtboxOwner.Player;
	}

	protected override void Initialize ()
	{
		//Default value for testing.  Eventually will load health and other stats somehow
		health = 10;
		PlayerReferenceHolder.AddPlayerReference (gameObject);
	}
}

//This enum is to control player movement speed through player controller means, such as abilities that trigger higher/lower movement speed.  
//Movement Debuffs should be handled in a different way if needed so stacking works appropriately
public enum PlayerMovementState
{
	Normal,
	Slow,
	Fast
}