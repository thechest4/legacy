﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class will read relevant player states and input states and initiate any gameplay actions such as attacks or abilities.
/// Those actions should be stored in other classes.
/// </summary>
public class PlayerActionsParser : Photon.PunBehaviour, IActionManager
{
	[SerializeField]
	private PlayerStateContainer playerStateContainer;

	[SerializeField]
	private Animator animator;

	[SerializeField]
	private GameObject actionPrefabRoot;

	public IPlayerControlledAction currentPlayerAction;

	//Making the assumption that each class will have one attack script.  May need to change this later
	//Also Serializing this just temporarily
	[SerializeField]
	private PlayerActionContainer playerAttackScript;

	void Start ()
	{
		if (photonView.isMine)
		{
			playerAttackScript.Result.Initialize(playerStateContainer, animator);
		}
		else
		{
			this.enabled = false;
		}
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (playerStateContainer.currentActionState == ActionState.Free)
		{
			//Ideally the logic here would not parse input states directly, but rather iterate through a collection of IPlayerControlledActions and call HasInputStateBeenMet
			if (playerStateContainer.currentInputState.attackWasPressed && playerAttackScript.Result.HasInputStateBeenMet ())
			{
				playerAttackScript.Result.PerformAction ();
				currentPlayerAction = playerAttackScript.Result;
			}
		}
	}

	public void FinishCurrentAction ()
	{
		if (currentPlayerAction != null)
		{
			currentPlayerAction.FinishAction ();
		}
	}

	public void CurrentActionCreateProjectile ()
	{
		if (currentPlayerAction != null)
		{
			IProjectileAction projectileAction = currentPlayerAction as IProjectileAction;

			if (projectileAction != null)
			{
				projectileAction.CreateProjectile ();
			}
		}
	}
}

