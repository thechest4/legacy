﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Holds names of animation keys used by players
/// </summary>
public class AnimationKeys
{
	public static string MOVEMENTAXIS_BOOL 		= "IsMoving";
	public static string MELEEATTACK_TRIGGER 	= "MeleeAttackTrigger";
	public static string FLINCH_TRIGGER			= "Flinch";
	public static string DIE_TRIGGER			= "Die";
	public static string PROJATTACK_TRIGGER		= "ProjectileAttackTrigger";
}

