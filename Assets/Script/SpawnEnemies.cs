﻿using UnityEngine;
using System.Collections;

public class SpawnEnemies : Photon.PunBehaviour
{
	[SerializeField]
	private EnemyStateContainer enemyStateContainer;

	[SerializeField]
	private string enemyPrefabName;

	[SerializeField]
	private float spawnInterval;

	[SerializeField]
	private Transform spawnPoint;

	private float currentSpawnTimer;
	
	// Update is called once per frame
	void Update ()
	{
		if (photonView.isMine && enemyStateContainer.currentHealthState != HealthState.Dying)
		{
			if (currentSpawnTimer >= spawnInterval)
			{
				SpawnEnemy ();
				currentSpawnTimer = 0.0f;
			}
			else
			{
				currentSpawnTimer += Time.deltaTime;
			}
		}
	}

	private void SpawnEnemy ()
	{
		PhotonNetwork.Instantiate (enemyPrefabName, spawnPoint.position, Quaternion.identity, 0);
	}
}

