﻿using UnityEngine;
using System.Collections;

public interface IProjectileAction
{
	void CreateProjectile ();
}

