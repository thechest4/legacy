﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Detects collision with a hurtbox and fires an event
/// </summary>
[RequireComponent(typeof(Collider))]
public class Hitbox : MonoBehaviour
{
	public static string HITBOX_LAYER_NAME = "Hitbox";

	public HitboxOwner hitboxOwner { get; private set; }

	private List<HurtboxOwner> validTargets;

	public event Action<Hurtbox> OnHurtboxHit;

	public void Initialize (HitboxOwner argHitboxOwner)
	{
		hitboxOwner = argHitboxOwner;
		gameObject.layer = LayerMask.NameToLayer (HITBOX_LAYER_NAME);
		gameObject.SetActive (false);

		GenerateValidTargets ();
	}

	private void GenerateValidTargets ()
	{
		validTargets = new List<HurtboxOwner> ();

		switch (hitboxOwner)
		{
			case HitboxOwner.Player:
				validTargets.Add (HurtboxOwner.Enemy);
				validTargets.Add (HurtboxOwner.Boss);
				break;
			case HitboxOwner.Enemy:
				validTargets.Add (HurtboxOwner.Player);
				break;
			case HitboxOwner.Boss:
				validTargets.Add (HurtboxOwner.Enemy);
				validTargets.Add (HurtboxOwner.Player);
				break;
		}
	}

	public void Activate ()
	{
		gameObject.SetActive (true);
	}

	public void Deactivate ()
	{
		gameObject.SetActive (false);
	}

	void OnTriggerEnter (Collider col)
	{
		Hurtbox hitHurtbox = col.gameObject.GetComponent<Hurtbox> ();
		if (validTargets.Contains (hitHurtbox.hurtboxOwnerType))
		{
			FireOnHurtboxHit (hitHurtbox);
		}
	}

	private void FireOnHurtboxHit (Hurtbox hitHurtbox)
	{
		if (OnHurtboxHit != null)
		{
			OnHurtboxHit (hitHurtbox);
		}
	}
}

//Player hitboxes can hit Enemies and Bosses
//Enemy hitboxes can hit Players
//Boss hitboxes can hit Players and Enemies
public enum HitboxOwner
{
	Player,
	Enemy,
	Boss
}