﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerReferenceHolder : MonoBehaviour
{
	public static List<GameObject> PlayerReferences;

	[SerializeField]
	private GameObject tempPlayerReference;

	// Use this for initialization
	void Start ()
	{
		PlayerReferences = new List<GameObject> ();
//		PlayerReferences.Add (tempPlayerReference);
	}

	public static List<GameObject> GetPlayerReferences ()
	{
		return PlayerReferences;
	}

	public static void AddPlayerReference (GameObject player)
	{
		PlayerReferences.Add (player);
	}
}

