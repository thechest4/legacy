﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyMeleeAttackBase : MonoBehaviour, IEnemyAction
{
	[SerializeField]
	protected HitboxManager hitboxManager;

	[SerializeField]
	protected float range = 0.1f;

	protected EnemyStateContainer enemyStateContainer;
	protected Animator animator;

	protected List<Hurtbox> hitHurtboxes;
	protected float cooldown;
	protected float lastUseTimestamp;

	public void Initialize (EnemyStateContainer argEnemyStateContainer, Animator argAnimator)
	{
		enemyStateContainer 	= argEnemyStateContainer;
		animator 				= argAnimator;

		hitHurtboxes = new List<Hurtbox> ();

		List<Hitbox> hitboxes = hitboxManager.GetHitboxes ();

		for (int i = 0; i < hitboxes.Count; i++)
		{
			hitboxes[i].Initialize (HitboxOwner.Enemy);
			hitboxes[i].OnHurtboxHit += HandleOnHurtboxHit;
		}
	}

	public virtual bool CanThisActionBePerformed (GameObject target)
	{
		bool isInRange = Vector3.Distance (transform.position, target.transform.position) <= range;

		return isInRange && IsAbilityOffCooldown ();
	}

	public void PerformAction (GameObject target)
	{
		enemyStateContainer.currentActionState = ActionState.Busy;
		animator.SetTrigger (AnimationKeys.MELEEATTACK_TRIGGER);
		hitboxManager.isActivated = true;
		hitHurtboxes.Clear ();
	}

	public virtual void FinishAction () 
	{
		enemyStateContainer.currentActionState = ActionState.Free;

		hitboxManager.isActivated = true;
		hitboxManager.DeactivateHitboxes ();
	}

	public virtual void InterruptAction () 
	{ 
		FinishAction ();
	}

	public bool IsAbilityOffCooldown ()
	{
		if (Time.time - lastUseTimestamp >= cooldown)
		{
			return true;
		}

		return false;
	}

	public virtual void Shutdown () 
	{	
		List<Hitbox> hitboxes = hitboxManager.GetHitboxes ();

		for (int i = 0; i < hitboxes.Count; i++)
		{
			hitboxes[i].OnHurtboxHit -= HandleOnHurtboxHit;
		}
	}

	protected virtual void HandleOnHurtboxHit (Hurtbox hitHurtbox) 
	{
		if (!hitHurtboxes.Contains (hitHurtbox))
		{
			//Can probably even move this out to the base class once damage can be calculated by stats
			hitHurtbox.ApplyDamageToStateContainer (2);
			hitHurtboxes.Add (hitHurtbox);
		}
	}

	void OnDestroy ()
	{
		Shutdown ();
	}
}

