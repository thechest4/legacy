﻿using UnityEngine;
using System.Collections;

public interface IEnemyAction 
{
	void Initialize (EnemyStateContainer argEnemyStateContainer, Animator argAnimator);

	bool CanThisActionBePerformed (GameObject target);

	void PerformAction (GameObject target);

	void FinishAction ();

	void InterruptAction ();

	bool IsAbilityOffCooldown ();

	void Shutdown ();
}

[System.Serializable]
public class EnemyActionContainer : IUnifiedContainer<IEnemyAction> { }
