﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyActionDecider : Photon.PunBehaviour, IActionManager
{
	[SerializeField]
	private EnemyStateContainer enemyStateContainer;

	[SerializeField]
	private List<EnemyActionContainer> enemyActions;

	[SerializeField]
	private Animator animator;

	private GameObject currentTarget;

	private IEnemyAction currentAction;

	void Start ()
	{
		if (photonView.isMine)
		{
			enemyStateContainer.OnDeath += HandleOnDeath;
			enemyStateContainer.OnDamageTaken += HandleOnDamageTaken;

			for (int i = 0; i < enemyActions.Count; i++)
			{
				enemyActions[i].Result.Initialize (enemyStateContainer, animator);
			}
		}
		else
		{
			this.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (enemyStateContainer.currentActionState == ActionState.Free)
		{
			if (currentTarget == null || ShouldChangeTarget ())
			{
				AcquireTarget ();
			}

			if (currentTarget != null)
			{
				for (int i = 0; i < enemyActions.Count; i++)
				{
					if (enemyActions[i].Result.CanThisActionBePerformed (currentTarget))
					{
						if (currentAction != null && enemyActions[i].Result != currentAction)
						{
							currentAction.FinishAction ();
						}

						enemyActions[i].Result.PerformAction (currentTarget);
						currentAction = enemyActions[i].Result;
						break;
					}
				}
			}
		}
	}

	public void FinishCurrentAction ()
	{
		if (currentAction != null)
		{
			currentAction.FinishAction ();
		}
	}

	private void AcquireTarget ()
	{
		currentTarget = PlayerReferenceHolder.GetPlayerReferences ()[0];
	}

	private bool ShouldChangeTarget ()
	{
		return false;
	}

	private void HandleOnDamageTaken ()
	{
		if (currentAction != null)
		{
			currentAction.InterruptAction ();
		}

		currentAction = null;
	}

	private void HandleOnDeath ()
	{
		enemyStateContainer.OnDeath -= HandleOnDeath;

		this.enabled = false;
	}

	void OnDestroy ()
	{
		enemyStateContainer.OnDeath -= HandleOnDeath;
		enemyStateContainer.OnDamageTaken -= HandleOnDamageTaken;
	}

	public void CurrentActionCreateProjectile ()
	{
		if (currentAction != null)
		{
			IProjectileAction projectileAction = currentAction as IProjectileAction;

			if (projectileAction != null)
			{
				projectileAction.CreateProjectile ();
			}
		}
	}
}

