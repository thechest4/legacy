﻿using UnityEngine;
using System.Collections;

/// <summary>
/// All enemy state information is stored here, and all player components check this component for state information.
/// </summary>
public class EnemyStateContainer : StateContainerBase
{
	protected override void SetHurtboxOwnerType ()
	{
		hurtboxOwnerType = HurtboxOwner.Enemy;
	}

	protected override void Initialize ()
	{
		//Default value for testing.  Eventually will load health and other stats somehow
		health = 4;
	}

	protected override void Die ()
	{
		if (photonView.isMine)
		{
			base.Die ();

			Invoke ("DestroyThisObject", 3.0f);
		}
	}

	protected void DestroyThisObject ()
	{
		PhotonNetwork.Destroy (gameObject);
	}
}