﻿using UnityEngine;
using System.Collections;

public class EnemyMovementAction : MonoBehaviour, IEnemyAction
{
	[SerializeField]
	private float moveSpeed = 0.5f;

	[SerializeField]
	private Rigidbody body;

	private Animator animator;

	public void Initialize (EnemyStateContainer argEnemyStateContainer, Animator argAnimator) 
	{ 
		animator = argAnimator;
	}

	public void PerformAction (GameObject target)
	{
		Vector3 direction = target.transform.position - transform.position;
		Vector3 translation = direction * moveSpeed * Time.deltaTime;

		body.MovePosition (transform.position + translation);

		if (translation.normalized.magnitude != 0.0f)
		{
			transform.forward = translation.normalized;
		}

		animator.SetBool (AnimationKeys.MOVEMENTAXIS_BOOL, true);
	}

	public bool CanThisActionBePerformed (GameObject target)
	{
		return true;
	}

	public void FinishAction () 
	{ 
		animator.SetBool (AnimationKeys.MOVEMENTAXIS_BOOL, false);
	}

	public void InterruptAction () 
	{
		FinishAction ();
	}

	public bool IsAbilityOffCooldown () 
	{
		return true; 
	}

	public void Shutdown () { }
}

