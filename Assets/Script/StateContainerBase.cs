﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Base class for state containers
/// </summary>
public abstract class StateContainerBase : Photon.PunBehaviour
{
	public int health { get; protected set; }

	public ActionState currentActionState { get; set; }
	public HealthState currentHealthState { get; set; }

	public HurtboxOwner hurtboxOwnerType { get; protected set; }

	public event Action OnDamageTaken;
	public event Action OnDeath;

	void Start ()
	{
		SetHurtboxOwnerType ();
		Initialize ();
	}

	[PunRPC]
	public virtual void ApplyDamage (int damage)
	{
		if (currentHealthState != HealthState.Dying)
		{
			health -= damage;

			if (health <= 0)
			{
				Die ();
			}
			else
			{
				FireOnDamageTaken ();
			}
		}
	}

	protected void FireOnDamageTaken ()
	{
		if (OnDamageTaken != null)
		{
			OnDamageTaken ();
		}
	}

	protected void FireOnDeath ()
	{
		if (OnDeath != null)
		{
			OnDeath ();
		}
	}

	protected virtual void Initialize () 
	{
		currentHealthState = HealthState.Healthy;
	}

	protected virtual void Die ()
	{
		currentHealthState = HealthState.Dying;
		FireOnDeath ();
	}

	protected abstract void SetHurtboxOwnerType ();
}

public enum ActionState
{
	Free,
	Busy
}

public enum HealthState 
{
	Healthy,
	Dying
}