﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Used by an attack to apply damage or status effects to a statecontainer
/// </summary>
[RequireComponent(typeof(Collider))]
public class Hurtbox : MonoBehaviour
{
	[SerializeField]
	private StateContainerBase stateContainer;
	
	public static string HURTBOX_LAYER_NAME = "Hurtbox";

	public HurtboxOwner hurtboxOwnerType { get; private set; }

	void Start ()
	{
		gameObject.layer = LayerMask.NameToLayer (HURTBOX_LAYER_NAME);
		gameObject.SetActive (true);

		DetermineHurtboxOwner ();
	}

	private void DetermineHurtboxOwner ()
	{
		hurtboxOwnerType = stateContainer.hurtboxOwnerType;
	}

	public void ApplyDamageToStateContainer (int damage)
	{
		PhotonNetwork.RPC (stateContainer.photonView, "ApplyDamage", PhotonTargets.AllViaServer, true, new object[] { damage });
//		stateContainer.ApplyDamage (damage);
	}

	public void ApplyStatusChange ()
	{
		//Use this to apply status effects or debuffs to the state container
	}
}

//Player hitboxes can hit Enemies and Bosses
//Enemy hitboxes can hit Players
//Boss hitboxes can hit Players and Enemies
public enum HurtboxOwner
{
	Player,
	Enemy,
	Boss
}

