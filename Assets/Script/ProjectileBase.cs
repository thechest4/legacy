﻿using UnityEngine;
using System;
using System.Collections;

public class ProjectileBase : Photon.PunBehaviour
{
	[SerializeField]
	protected Hitbox hitbox;

	[SerializeField]
	private Rigidbody body;

	[SerializeField]
	protected float lifetime = 2.0f;

	[SerializeField]
	protected float velocity;

	public event Action<Hurtbox> OnProjectileHitHurtbox;

	protected Vector3 direction;
	protected float spawnTimestamp;

	public void Initialize (Vector3 argDirection)
	{
		if (!photonView.isMine)
		{
			this.enabled = false;
		}

		direction = argDirection;
		spawnTimestamp = Time.time;

		hitbox.OnHurtboxHit += HandleOnHurtboxHit;
		hitbox.Initialize (HitboxOwner.Player);
		hitbox.Activate ();

		transform.forward = direction;
	}

	void FixedUpdate ()
	{
		UpdateProjectileMovement ();
	}

	void Update ()
	{
		if (Time.time - spawnTimestamp >= lifetime)
		{
			DestroyProjectile ();
		}
	}

	protected virtual void UpdateProjectileMovement ()
	{
		Vector3 translation = transform.forward * velocity * Time.fixedDeltaTime;
		body.MovePosition (transform.position + translation);
	}

	protected virtual void DestroyProjectile ()
	{
		Shutdown ();
		PhotonNetwork.Destroy (gameObject);
	}

	protected virtual void HandleOnHurtboxHit (Hurtbox hitHurtbox)
	{
		FireOnProjectileHitHurtbox (hitHurtbox);
		DestroyProjectile ();
	}

	protected void FireOnProjectileHitHurtbox (Hurtbox hitHurtbox)
	{
		if (OnProjectileHitHurtbox != null)
		{
			OnProjectileHitHurtbox (hitHurtbox);
		}
	}

	protected virtual void Shutdown ()
	{
		hitbox.OnHurtboxHit -= HandleOnHurtboxHit;
		OnProjectileHitHurtbox = null;
	}

	void OnCollisionEnter (Collision col)
	{
		DestroyProjectile ();
	}
}

