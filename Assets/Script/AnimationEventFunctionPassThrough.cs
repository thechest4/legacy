﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script only exists as a passthrough so that animation events can call functions not on the same gameobject
/// </summary>
public class AnimationEventFunctionPassThrough : MonoBehaviour
{
	[SerializeField]
	private List<HitboxManager> hitboxManagers;

	[SerializeField]
	private ActionManagerContainer actionManagerContainer;

	public void ActivateHitbox (int index)
	{
		for (int i = 0; i < hitboxManagers.Count; i++)
		{
			if (hitboxManagers[i].isActivated)
			{
				hitboxManagers[i].ActivateHitbox (index);
				break;
			}
		}
	}

	public void DeactivateHitboxes ()
	{
		for (int i = 0; i < hitboxManagers.Count; i++)
		{
			if (hitboxManagers[i].isActivated)
			{
				hitboxManagers[i].DeactivateHitboxes ();
			}
		}
	}

	public void FinishCurrentAction ()
	{
		actionManagerContainer.Result.FinishCurrentAction ();
	}

	public void CurrentActionCreateProjectile ()
	{
		actionManagerContainer.Result.CurrentActionCreateProjectile ();
	}
}

